var mongoose = require('mongoose');

module.exports = mongoose.model('Rate', {
    currency: { type: String, required: false },
    symbol: { type: String, required: false },
    rate: { type: String, required: false },
});