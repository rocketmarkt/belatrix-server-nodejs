var mongoose = require('mongoose');

module.exports = mongoose.model('Exchange', {
	currency: { type: String, required: false },
    symbol: { type: String, required: false },
	rate: { type: Number, required: false },
	date: { type: Date, required: true },
});