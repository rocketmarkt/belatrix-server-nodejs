var mongoose = require('mongoose');

module.exports = mongoose.model('LogError', {
	schemaName: String,
	function: String,
	message: String,
	date: Date
});
