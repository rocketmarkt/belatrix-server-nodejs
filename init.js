var Exchange = require('./models/exchange');

   //Create Inital table of rates

    var saveUSD = new Exchange({
        currency: 'Dollar',
        symbol: 'USD',
        rate: 1,
        date: new Date()
    }).save(function (err) {       
        if (err) {
            console.log("save USD error", err);      
        } else {
            console.log("status USD Saved");          
        }
    });

    var saveEUR = new Exchange({
        currency: 'Euro',
        symbol: 'EUR',
        rate: 0.87912,
        date: new Date()
    }).save(function (err) {
        if (err) {
            console.log("save EUR error", err);      
        } else {
            console.log("status EUR Saved");          
        }
    });

    var savePEN = new Exchange({
        currency: 'Soles',
        symbol: 'PEN',
        rate: 3.3343,
        date: new Date()
    }).save(function (err) {
        if (err) {
            console.log("save PEN error", err);      
        } else {
            console.log("status PEN Saved");          
        }
    });