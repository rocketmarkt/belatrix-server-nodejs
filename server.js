var http = require('http'),
	exchanges = require('./data'),
	db = require('./db'),
	app = require('./app')(exchanges);
	schedule = require('./schedule');
	init = require('./init');


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});