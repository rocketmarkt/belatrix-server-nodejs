module.exports = {
	"EUR": {
		"base": 'USD',
		"date": "2018-06-11",
		"rate": 0.89912
	},
	"USD": {
		"base": 'EUR',
		"date": "2018-06-11",
		"rate": 0.89912
	}
};