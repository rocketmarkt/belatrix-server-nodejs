const request = require('supertest');
// const app = require('../../app');
const expect = require('chai').expect;
const mongoose = require('mongoose');

const PORT = process.env.PORT || 3000;
const HOST = `http://localhost:${PORT}`;
// const someController = require('../controller/someController');
const Exchange = require('../models/exchange');

describe('Routing and Integration Tests', () => {
    describe('Controller Tests', () => {

        before('connect', function () {
            return mongoose.createConnection('mongodb://test:test123@ds153593.mlab.com:53593/belatrix', { useNewUrlParser: true })
        })

        it('should show the exchange rate USD to EUR GET', function (done) {
            request(HOST)
                .get('/exchange?base=USD&symbols=EUR')
                .end((err, res) => {
                    if (err) done(err);

                    expect(res.body).to.be.an('object');
                    done();
                });
        });

        it('should accept valid data and return 201 status with Created object', (done) => {
            const goodReq = {
                schemaName: 'Test SchemaName ',
                function: 'Test Function',
                message: 'Test Message.',
                date: new Date(),
            };
            
            request(HOST)
                .post('/logerrors')
                .send(goodReq)               
                .expect(201, done);               
        });

        it ('should reject resoruce doesn´t fond with 404 status', (done) => {
            const badReq = {
              notAJob: 'not real data',
            };
            request(HOST)
              .post('/otherresource')
              .send(badReq)
              .expect(404, done);              
          });

        after(() => {
            mongoose.connection.close(() => {
                console.log('Test database connection closed');
            });
        });

    });
});