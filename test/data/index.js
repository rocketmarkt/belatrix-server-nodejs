module.exports = {
	"EUR": {
		"base": 'USD',
		"date": "2018-06-11",
		"rate": 0.89912
	},
	"USD": {
		"base": 'EUR',
		"date": "2018-06-11",
		"rate": 1.137501
	},
	"USD": {
		"base": 'PEN',
		"date": "2018-06-11",
		"rate": 0.2999
	},
	"PEN": {
		"base": 'USD',
		"date": "2018-06-11",
		"rate": 3.2423
	},
	"EUR": {
		"base": 'PEN',
		"date": "2018-06-11",
		"rate": 0.2553
	},
	"PEN": {
		"base": 'EUR',
		"date": "2018-06-11",
		"rate": 3.8159
	}
};