
/*
 * GET home page.
 */

var LogError = require('../models/logError');
var Exchange = require('../models/exchange');

module.exports = function (exchanges) {
	var exchange = require('../exchange');

	///------ Testing date without connection 

	// for (var base in exchanges) {
	// 	exchanges[base] = exchange(exchanges[base]);
	// }


	var functions = {};

	//GET QUERY STRING REQUESTED IN THE EXERCISE
	functions.exchange = function (req, res) {
		var base = req.query.base;
		var symbols = req.query.symbols;
		
		//Calling function in order get the converted rates 
		getRates(base, symbols, res);

	};

	//POST
	functions.saveError = function (req, res) {
		
		//Save Log Error
		var saveLogError = new LogError({
			schemaName: req.body.schemaname,
			function: req.body.function,
			message: req.body.message,
			date: new Date()
		}).save()
			.then( (LogErrorPost, err) => {
				if (err) {
					res.status(400).json({
						message: "should reject invalid data"
					});
				} else {
					res.status(201).json({
						message: "LogError added successfully",
						LogError: {
							...LogErrorPost,
							id: LogErrorPost._id
						}
					});
				}
			})
			.catch(err => {
				//Save Log Error in Database MongoDB when the call fail
				var saveLogError = new LogError({
					schemaName: 'Log Errors',
					function: 'saveError',
					message: 'message: ' + err,
					date: new Date()
				}).save();

				res.status(500).json({
					message: "Creating a LogError failed!"	
				});
			});

	};

	//GET ALL LOG ERRORS
	functions.logErrors = function (req, res) {

		LogError.find({}).then((logErrors, err) => {
			if (err) {
				res.status(500).json({ status: 'failure' });
			} else {
				res.status(200).json({
					message: "logErrors fetched successfully!",
					logErrors: logErrors
				});
			}
		});
	}

	//Return all functions 
	return functions;

};


//Additional Functions

function getRates(pBase, pSymbols, res) {
	var rateBase, rateSymbols;

	Exchange.findOne({ symbol: pBase }, {}, { sort: { 'date': -1 } })
		.then((exchange, err) => {
			if (exchange) {
				var recordBase = new Exchange(
					exchange
				);
				rateBase = recordBase.rate;
			} else {
				res.status(500).json({
					message: "Base wasn´t found!" + err
				});
			}
			//Return rateBase, this will be used in the next steps to calculate rateConverted
			return rateBase;
		})
		.then((rateBaseResolved) => {
			Exchange.findOne({ symbol: pSymbols }, {}, { sort: { 'date': -1 } })
				.then((exchange, err) => {

					if (exchange) {
						var recordSymbols = new Exchange(
							exchange
						);
						rateSymbols = recordSymbols.rate;

						//Converter rate - Factor: base/symbols
						var rateConverted = rateBaseResolved / rateSymbols;

						//Using template literals in order to create object dynamically
						var rateObj = `{"${pSymbols}":"${rateConverted}"}`;

						//response rates
						res.status(200).json({
							base: pBase,
							date: formatDate(new Date()),
							rate: JSON.parse(rateObj)
						});
					} else {
						res.status(500).json({
							message: "Symbols wasn´t found!" + err
						});
					}
				})
				.catch(err => {
					//Save Log Error in Database MongoDB when the call fail
					var saveLogError = new LogError({
						schemaName: 'Exchange',
						function: 'Rate Symbol',
						message: 'message: ' + err,
						date: new Date()
					}).save();

					res.status(500).json({
						message: "Fetching symbol failed!" + err
					});
				});
		})
		.catch(err => {
			//Save Log Error in Database MongoDB when the call fail
			var saveLogError = new LogError({
				schemaName: 'Exchange',
				function: 'Rate Base',
				message: 'message: ' + err,
				date: new Date()
			}).save();

			//Return error
			res.status(500).json({
				message: "Fetching Base failed!" + err
			});
		});

}


function formatDate(d) {
	var month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
}
