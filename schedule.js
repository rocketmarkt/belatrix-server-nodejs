var schedule = require('node-schedule');

var Exchange = require('./models/exchange');

var rule = new schedule.RecurrenceRule();

//Run job every (10 minutes) - FOR TESTING REASON JOB RUN ONLY EVERY 1 MINUTO
rule.minute = new schedule.Range(0, 59, 1);


schedule.scheduleJob(rule, function () {
	//Only will be updated EUR & PEN, the USD is the base rate continue 1
	updateRate("EUR");
	updateRate("PEN");
	console.log('Rates were updated!---------------------------');
});


//Additional Functions
function updateRate(symbol) {
	//Generate random variation of rates
	var variation = Math.random() * (0.100 - 0.0500) - 0.0500;

	//We have that make sure that only one will be updated
	Exchange.findOne({ symbol: symbol }, {}, { sort: { 'date': -1 } })
	.then((exchange, err) => {
		if (exchange) {
			var record = new Exchange(
				exchange
			);
			console.log('record.rate 0!---------------------------' + record.rate.toString());				
			//Update rate randomly
			record.rate = record.rate + variation;
			console.log('record.rate 1!---------------------------' + record.rate.toString());
			record.save(function (err) {
					if (err) {
						console.log("updated " + symbol + " error", err);
					} else {
						console.log("rate " + symbol + " updated");
					}
			});
		} else {
			console.log("Symbol wasn´t found!", err);
		}	
	});
}

module.exports = schedule;