**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

# Description of Project

## In this project the following technologies were used:
- Node js
- Express
- Scheduler(The document indicates that it must run every 10 min, but to speed up the testing it is running every minute).
- Mongo DB
- Test using Mocha

This project has the following rest apis.

## You can conveter rates in 3 currency: USD, EUR, PEN
- GET: http://localhost:3000/exchange?base=EUR&symbols=USD 
- GET:http://localhost:3000/exchange?base=PEN&symbols=EUR
- GET: http://localhost:3000/exchange?base=USD&symbols=PEN

## You can also get all list of Log errors
- GET:http://localhost:3000/logerrors

## Additionality you can create a log error by  rest api
- POST: http://localhost:3000/logerrors
 Body:
 {
    "schemaname": "This a SchemaName",
    "function": " This a Function",
    "message": " This a Message."
 }

## Run Project
- use: npm run start

## Run Tests
- use: mocha


