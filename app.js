
/**
 * Module dependencies.
 */

module.exports = function (exchanges) {
	var express = require('express');
	var routes = require('./routes')(exchanges);
	var path = require('path');	
	var app = express();

	app.use(express.json());	

	// all environments
	app.set('port', process.env.PORT || 3000);
	
	
	//Calls
	app.get('/exchange', routes.exchange);
	app.post('/logerrors', routes.saveError);
	app.get('/logerrors', routes.logErrors);	

	return app;
}


